This dataset is from https://github.com/cocoxu/SemEval-PIT2015 which is released for non-commercial use under the CC BY-NC-SA 3.0 license.

Please check the evaluation and citation details in their repository.