import json
from sklearn.model_selection import train_test_split
import os

def get_review_text(review):
    """
    Get a string containing the title and body of the review
    :param article: A Yelp review dict
    :return: String containing the review title and body
    """
    body = ""
    if review['text'] is not None:
        body = ' '.join(review['text'].split())
    return body


def get_binary_label(label):
    category_label = [0 for x in range(5)]
    category_label[label - 1] = 1
    return ''.join(map(str, category_label))


def parse_documents():
    """
    Extract the reviews from Yelp dataset and create train/dev/test splits
    :return: Three lists containing the train, dev and test splits along with the labels
    """
    with open(os.path.join("data", "reviews.json"), 'r') as json_file:
        reviews = list()
        labels = list()
        for json_line in json_file:
            review = json.loads(json_line)
            labels.append(get_binary_label(review['stars']))
            reviews.append(get_review_text(review))
    x_train, x_test, y_train, y_test = train_test_split(reviews, labels, test_size=0.1, random_state=37, stratify=labels)
    x_train, x_dev, y_train, y_dev = train_test_split(x_train, y_train, test_size=0.11, random_state=53, stratify=y_train)
    return x_train, y_train, x_dev, y_dev, x_test, y_test


if __name__ == "__main__":
    x_train, y_train, x_dev, y_dev, x_test, y_test = parse_documents()
    print("Train, dev and test dataset sizes:", len(x_train), len(x_dev), len(x_test))
    with open(os.path.join("data", "yelp2018_train.tsv"), 'w', encoding='utf8') as tsv_file:
        for label, document in zip(y_train, x_train):
            tsv_file.write(label + "\t" + document + "\n")
    with open(os.path.join("data", "yelp2018_validation.tsv"), 'w', encoding='utf8') as tsv_file:
        for label, document in zip(y_dev, x_dev):
            tsv_file.write(label + "\t" + document + "\n")
    with open(os.path.join("data", "yelp2018_test.tsv"), 'w', encoding='utf8') as tsv_file:
        for label, document in zip(y_test, x_test):
            tsv_file.write(label + "\t" + document + "\n")
