def process(split):
    f = open("raw_data/Twitter_URL_Corpus_{}.txt".format(split))
    fa = open("{}/a.toks".format(split), "w")
    fb = open("{}/b.toks".format(split), "w")
    fsim = open("{}/sim.txt".format(split), "w")
    faid = open("{}/aid.txt".format(split), "w")
    fbid = open("{}/bid.txt".format(split), "w")
    a2id = {}
    b2id = {}
    aid = 0
    bid = 0
    for l in f:
        ls = l.split("\t")
        if len(ls) <= 3:
            continue
        a = ls[0]
        b = ls[1]
        score = int(ls[2][1])
        if score > 3:
            sim = 1
        elif score < 3:
            sim = 0
        else:
            continue
        if a not in a2id:
            a2id[a] = aid
            aid += 1
        if b not in b2id:
            b2id[b] = bid
            bid += 1
        assert len(a) > 0
        assert len(b) > 0
        fa.write("{}\n".format(a))
        fb.write("{}\n".format(b))
        fsim.write("{}\n".format(sim))
        faid.write("{}\n".format(a2id[a]))
        fbid.write("{}\n".format(b2id[b]))

    f.close()
    fa.close()
    fb.close()
    fsim.close()
    faid.close()
    fbid.close()    

if __name__ == '__main__':
    for split in ["train", "test"]:
        process(split)
